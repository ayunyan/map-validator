package jp.minecraft.mapvalidator;

import lombok.Getter;
import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ayu on 2015/01/16.
 */
public class ValidationErrorHandler implements ErrorHandler {
    @Getter
    private List<SAXParseException> exceptions = new ArrayList<SAXParseException>();

    @Override
    public void warning(SAXParseException exception) throws SAXException {
        exceptions.add(exception);
    }

    @Override
    public void error(SAXParseException exception) throws SAXException {
        exceptions.add(exception);
    }

    @Override
    public void fatalError(SAXParseException exception) throws SAXException {
        exceptions.add(exception);
    }
}

package jp.minecraft.mapvalidator;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

/**
 * Created by ayu on 2015/01/16.
 */
public class MapValidator {
    private String schemaUrl = "https://maps.minecraft.jp/map.xsd";
    private Schema schema;
    private JFrame frame;
    private JTextArea textArea;

    public MapValidator() {
        initializeGUI();
        loadSchema();
    }

    private void initializeGUI() {
        frame = new JFrame("Map Validator");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setBounds(100, 100, 600, 300);
        try {
            BufferedImage icon = ImageIO.read(MapValidator.class.getResource("/main.png"));
            frame.setIconImage(icon);
        } catch (IOException e) {
            e.printStackTrace();
        }
        frame.setVisible(true);

        JScrollPane scrollPane = new JScrollPane();
        frame.getContentPane().add(scrollPane, BorderLayout.CENTER);

        textArea = new JTextArea();
        textArea.setLineWrap(true);
        scrollPane.setViewportView(textArea);

        textArea.setTransferHandler(new FileTransferHandler(this));

        JLabel statusBar = new JLabel("map.xmlをウィンドウにドロップしてください。");
        frame.add(statusBar, BorderLayout.PAGE_END);
    }

    private void loadSchema() {
        try {
            URL schemaFile = new URL(schemaUrl);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

            schema = schemaFactory.newSchema(schemaFile);
        } catch (MalformedURLException e) {
            addLog(e.getMessage());
        } catch (SAXException e) {
            addLog(e.getMessage());
        }

    }

    public void clearLog() {
        textArea.setText("");
    }

    public void addLog(String message) {
        textArea.setText(textArea.getText() + message + "\n");
    }

    public void validate(File file) {
        //initializeSchema();
        if (schema == null) {
            addLog("Schemaが利用出来ません。");
            return;
        }
        Source xmlFile = new StreamSource(file);
        Validator validator = schema.newValidator();
        ValidationErrorHandler errorHandler = new ValidationErrorHandler();
        validator.setErrorHandler(errorHandler);
        try {
            validator.validate(xmlFile);
            List<SAXParseException> exceptions = errorHandler.getExceptions();
            if (exceptions.size() == 0) {
                addLog(file.getAbsolutePath() + "の検証に成功しました。");
            } else {
                for (SAXParseException e: exceptions) {
                    addLog(String.valueOf(e.getLineNumber()) + "行目: " + e.getMessage());
                }
                addLog(file.getAbsolutePath() + "の検証に失敗しました。");
            }
        } catch (SAXException e) {
            addLog(file.getAbsolutePath() + "の検証に失敗しました。\n" + e.getLocalizedMessage());
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            validator.reset();
        }
    }

    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                MapValidator mainForm = new MapValidator();
            }
        });
    }
}
